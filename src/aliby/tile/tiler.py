"""
Tiler: Divides images into smaller tiles.

The tasks of the Tiler are selecting regions of interest, or tiles, of
images - with one trap per tile, correcting for the drift of the microscope
stage over time, and handling errors and bridging between the image data
and Aliby’s image-processing steps.

Tiler subclasses deal with either network connections or local files.

To find tiles, we use a two-step process: we analyse the bright-field image
to produce the template of a trap, and we fit this template to the image to
find the tiles' centres.

We use texture-based segmentation (entropy) to split the image into
foreground -- cells and traps -- and background, which we then identify with
an Otsu filter. Two methods are used to produce a template trap from these
regions: pick the trap with the smallest minor axis length and average over
all validated traps.

A peak-identifying algorithm recovers the x and y-axis location of traps in
the original image, and we choose the approach to template that identifies
the most tiles.

The experiment is stored as an array with a standard indexing order of
(Time, Channels, Z-stack, X, Y).
"""

import logging
import re
import typing as t
import warnings
from functools import lru_cache
from pathlib import Path

import dask.array as da
import h5py
import numpy as np
from agora.abc import ParametersABC, StepABC
from agora.io.writer import BridgeH5
from aliby.global_settings import imaging_specifications
from aliby.tile.process_traps import segment_traps
from aliby.tile.tiles import Tile, TileLocations
from skimage.registration import phase_cross_correlation


class TilerParameters(ParametersABC):
    """Define default values for tile size and the reference channels."""

    _defaults = {
        "tile_size": imaging_specifications["tile_size"],
        "ref_channel": "Brightfield",
        "ref_z": 0,
        "position_name": None,
        "magnification": imaging_specifications["magnification"],
        "initial_tp": 0,
    }


class Tiler(StepABC):
    """
    Divide images into smaller tiles for faster processing.

    Find tiles and re-register images if they drift.
    Fetch images from an OMERO server if necessary.

    Uses an Image instance, which lazily provides the pixel data,
    and, as an independent argument, image_metadata.
    """

    def __init__(
        self,
        image: da.core.Array,
        image_metadata: t.Dict,
        parameters: TilerParameters,
        tile_locations=None,
        microscopy_metadata: t.Dict = None,
    ):
        """
        Initialise.

        Parameters
        ----------
        image: an instance of Image
        image_metadata: dictionary
        parameters: an instance of TilerParameters
        tile_locs: (optional)
        micrscopy_metadata: optional
        """
        super().__init__(parameters)
        self.image = image
        self.position_name = parameters.to_dict()["position_name"]
        if "channels" in image_metadata and isinstance(image, da.Array):
            # information for a particular image
            self.channels = image_metadata["channels"]
        elif "channels_by_position" in microscopy_metadata["full"]:
            # likely zarr array
            self.channels = microscopy_metadata["full"][
                "channels_by_position"
            ][self.position_name]
        else:
            self.channels = list(range(image_metadata.get("size_c", 0)))
        # get spatial location of position
        if microscopy_metadata is not None:
            self.spatial_location = microscopy_metadata["full"][
                "spatial_locations"
            ][self.position_name]
        else:
            self.spatial_location = None
        # get reference channel - used for segmentation
        self.ref_channel_index = self.channels.index(parameters.ref_channel)
        self.tile_locs = tile_locations
        # adjust for non-standard magnification
        if self.tile_size != imaging_specifications["tile_size"]:
            print(
                "Warning: tile_size has been changed."
                "\nConsider changing magnification instead."
            )
        else:
            # adjust tile_size for any magnification differing from default
            self.tile_size = int(
                self.tile_size
                * self.magnification
                / imaging_specifications["magnification"]
            )

    @classmethod
    def from_image(
        cls,
        image,
        parameters: TilerParameters,
        microscopy_metadata: t.Dict = None,
    ):
        """
        Instantiate from an Image instance.

        Parameters
        ----------
        image: an instance of Image
        parameters: an instance of TilerPameters
        """
        return cls(
            image.data,
            image_metadata=image.metadata,
            parameters=parameters,
            microscopy_metadata=microscopy_metadata,
        )

    @classmethod
    def from_h5(
        cls,
        image,
        filepath: t.Union[str, Path],
        parameters: t.Optional[TilerParameters] = None,
        microscopy_metadata: t.Dict = None,
    ):
        """
        Instantiate from an h5 file.

        Parameters
        ----------
        image: an instance of Image
        filepath: Path instance
            Path to an h5 file.
        parameters: an instance of TileParameters (optional)
        """
        tile_locs = TileLocations.read_h5(filepath)
        image_metadata = BridgeH5(filepath).meta_h5
        image_metadata["channels"] = image.metadata["channels"]
        if parameters is None:
            parameters = TilerParameters.default()
        elif isinstance(parameters, dict):
            parameters = TilerParameters.default(**parameters)
        else:
            raise Exception(f"Tiler: parameters unrecognised - {parameters}")
        tiler = cls(
            image.data,
            image_metadata,
            parameters,
            tile_locations=tile_locs,
        )
        if hasattr(tile_locs, "drifts"):
            tiler.no_processed = len(tile_locs.drifts)
        return tiler

    @lru_cache(maxsize=2)
    def load_image(self, tp: int, c: int) -> np.ndarray:
        """
        Load image using dask.

        Assumes the image is arranged as
            no of time points
            no of channels
            no of z stacks
            no of pixels in y direction
            no of pixels in x direction

        Parameters
        ----------
        tp: integer
            An index for a time point
        c: integer
            An index for a channel

        Returns
        -------
        full: an array of images
        """
        full = self.image[tp + self.initial_tp, c]
        if hasattr(full, "compute"):
            # if using dask fetch images
            full = full.compute(scheduler="synchronous")
        return full

    @property
    def shape(self):
        """
        Return the shape of the image array.

        The image array is arranged as number of images, number of channels,
        number of z sections, and size of the image in y and x.
        """
        return self.image.shape

    @property
    def no_processed(self):
        """Return the number of processed images."""
        if not hasattr(self, "_no_processed"):
            self._no_processed = 0
        return self._no_processed

    @no_processed.setter
    def no_processed(self, value):
        self._no_processed = value

    @property
    def no_tiles(self):
        """Return number of tiles."""
        return len(self.tile_locs)

    def initialise_tiles(self, tile_size: int = None):
        """
        Find initial positions of tiles.

        Remove tiles that are too close to the edge of the image
        so no padding is necessary.

        Parameters
        ----------
        tile_size: integer
            The size of a tile.
        """
        initial_image = self.image[
            self.initial_tp, self.ref_channel_index, self.ref_z
        ]
        if tile_size:
            half_tile = tile_size // 2
            # max_size is the minimum of the numbers of x and y pixels
            max_size = min(self.image.shape[-2:])
            # find the tiles
            tile_locs = segment_traps(initial_image, tile_size)
            # keep only tiles that are not near an edge
            tile_locs = [
                [x, y]
                for x, y in tile_locs
                if half_tile < x < max_size - half_tile
                and half_tile < y < max_size - half_tile
            ]
            # store tiles in an instance of TileLocations
            self.tile_locs = TileLocations.from_tiler_init(
                tile_locs, tile_size
            )
        else:
            # one tile with its centre at the image's centre
            yx_shape = self.image.shape[-2:]
            tile_locs = [[x // 2 for x in yx_shape]]
            self.tile_locs = TileLocations.from_tiler_init(
                tile_locs, max_size=min(yx_shape)
            )

    def find_drift(self, tp: int):
        """
        Find any translational drift between two images.

        Use cross correlation between two consecutive images.

        Arguments
        ---------
        tp: integer
            Index for a time point.
        """
        prev_tp = max(0, tp - 1)
        # cross-correlate
        drift, _, _ = phase_cross_correlation(
            self.image[prev_tp, self.ref_channel_index, self.ref_z],
            self.image[tp, self.ref_channel_index, self.ref_z],
        )
        # store drift
        if 0 < tp < len(self.tile_locs.drifts):
            self.tile_locs.drifts[tp] = drift.tolist()
        else:
            self.tile_locs.drifts.append(drift.tolist())

    def get_tp_data(self, tp, c) -> np.ndarray:
        """
        Return all tiles corrected for drift.

        Parameters
        ----------
        tp: integer
            An index for a time point
        c: integer
            An index for a channel

        Returns
        ----------
        Numpy ndarray of tiles with shape (no tiles, z-sections, y, x)
        """
        tiles = []
        full = self.load_image(tp, c)
        for tile in self.tile_locs:
            # pad tile if necessary
            ndtile = Tiler.get_tile_and_pad(full, tile.as_range(tp))
            tiles.append(ndtile)
        return np.stack(tiles)

    def get_tile_data(self, tile_id: int, tp: int, c: int):
        """
        Return a tile corrected for drift and padding.

        Parameters
        ----------
        tile_id: integer
            Index of tile.
        tp: integer
            Index of time points.
        c: integer
            Index of channel.

        Returns
        -------
        ndtile: array
            An array of (x, y) arrays, one for each z stack
        """
        full = self.load_image(tp, c)
        tile = self.tile_locs.tiles[tile_id]
        ndtile = self.get_tile_and_pad(full, tile.as_range(tp))
        return ndtile

    def _run_tp(self, tp: int):
        """
        Find tiles for a given time point.

        Determine any translational drift of the current image from the
        previous one.

        Arguments
        ---------
        tp: integer
            The time point to tile.
        """
        if self.no_processed == 0 or not hasattr(self.tile_locs, "drifts"):
            self.initialise_tiles(self.tile_size)
        if hasattr(self.tile_locs, "drifts"):
            drift_len = len(self.tile_locs.drifts)
            if self.no_processed != drift_len:
                warnings.warn(
                    "Tiler: the number of processed tiles and the number of drifts"
                    " calculated do not match."
                )
                self.no_processed = drift_len
        # determine drift for this time point and update tile_locs.drifts
        self.find_drift(tp)
        # update no_processed
        self.no_processed = tp + 1
        # return result for writer
        return self.tile_locs.to_dict(tp)

    def run(self, time_dim=None):
        """Tile all time points in an experiment at once."""
        if time_dim is None:
            time_dim = 0
        for frame in range(self.image.shape[time_dim]):
            self.run_tp(frame)
        return None

    def get_tiles_timepoint(
        self, tp: int, channels=None, z: int = 0
    ) -> np.ndarray:
        """
        Get a multidimensional array with all tiles for a set of channels
        and z-stacks.

        Used by extractor.

        Parameters
        ---------
        tp: int
            Index of time point
        tile_shape: int or tuple of two ints
            Size of tile in x and y dimensions
        channels: string or list of strings
            Names of channels of interest
        z: int
            Index of z-channel of interest

        Returns
        -------
        res: array
            Data arranged as (tiles, channels, Z, X, Y)
        """
        if channels is None:
            channels = [0]
        elif isinstance(channels, str):
            channels = [channels]
        # convert to indices
        channels = [
            (
                self.channels.index(channel)
                if isinstance(channel, str)
                else channel
            )
            for channel in channels
        ]
        # get the data as a list of length of the number of channels
        res = []
        for c in channels:
            # only return requested z
            tiles = self.get_tp_data(tp, c)[:, z]
            # insert new axis at index 1 for missing time point
            tiles = np.expand_dims(tiles, axis=1)
            res.append(tiles)
        # stack at time-point axis if more than one channel
        final = np.stack(res, axis=1)
        return final

    def get_channel_index(self, channel: str or int) -> int or None:
        """
        Find index for channel using regex.

        If channels are strings, return the first matched string.
        If channels are integers, return channel unchanged if it is
        an integer.

        Parameters
        ----------
        channel: string or int
            The channel or index to be used.
        """
        if isinstance(channel, int) and all(
            map(lambda x: isinstance(x, int), self.channels)
        ):
            return channel
        elif isinstance(channel, str):
            return find_channel_index(self.channels, channel)
        else:
            return None

    @staticmethod
    def get_tile_and_pad(image_array, slices):
        """
        Pad slices if out of bounds.

        Parameters
        ----------
        full: array
            Slice of image (zstacks, x, y) - the entire position
            with zstacks as first axis
        slices: tuple of two slices
            Delineates indices for the x- and y- ranges of the tile.

        Returns
        -------
        tile: array
            A tile with all z stacks for the given slices.
            If some padding is needed, the median of the image is used.
            If much padding is needed, a tile of NaN is returned.
        """
        # number of pixels in the y direction
        max_size = image_array.shape[-1]
        # ignore parts of the tile outside of the image
        y, x = [slice(max(0, s.start), min(max_size, s.stop)) for s in slices]
        # get the tile including all z stacks
        tile = image_array[:, y, x]
        # find extent of padding needed in x and y
        padding = np.array(
            [(-min(0, s.start), -min(0, max_size - s.stop)) for s in slices]
        )
        if padding.any():
            tile_size = slices[0].stop - slices[0].start
            if (padding > tile_size / 4).any():
                # fill with NaN because too much of the tile is outside of the image
                tile = np.full(
                    (image_array.shape[0], tile_size, tile_size), np.nan
                )
            else:
                # pad tile with median value of the tile
                tile = np.pad(tile, [[0, 0]] + padding.tolist(), "median")
        return tile


def find_channel_index(image_channels: t.List[str], channel_regex: str):
    """Use a regex to find the index of a channel."""
    for index, ch in enumerate(image_channels):
        found = re.match(channel_regex, ch, re.IGNORECASE)
        if found:
            if len(found.string) - (found.endpos - found.start()):
                logging.getLogger("aliby").log(
                    logging.WARNING,
                    f"Channel {channel_regex} matched {ch} using regex",
                )
            return index


def find_channel_name(image_channels: t.List[str], channel_regex: str):
    """Find the name of the channel using regex."""
    index = find_channel_index(image_channels, channel_regex)
    if index is not None:
        return image_channels[index]
